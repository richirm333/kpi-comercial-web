let libs = [];


/*************************************************/
(function cleanLibs(libs = []) {
	try {
		const editJsonFile = require('edit-json-file');
		const rimraf = require('rimraf');

		let file = editJsonFile('./package-lock.json');
			
		libs.forEach(libName => {

			// Elimina del package-lock.json
			file.unset('dependencies.' + libName);
			
			// Elimina del node_modules/
			rimraf('./node_modules/' + libName, () => {}, () => {});
		})

		file.save();
	} catch (err) {}
})(libs);