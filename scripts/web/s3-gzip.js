const glob = require('glob');
const fs = require('fs');

// console.log('>>>>>>>> COMPRIMIENDO FILES CON GZIP\n');

glob( './dist/**/*', function( err, files ) {
	files.forEach(pathFile => {
		if(!fs.lstatSync(pathFile).isDirectory()) {
			fs.rename(pathFile, pathFile.replace('.gz', ''), () => {});
		}
	});
	
	console.log('<<<<<<<< COMPRESION FINALIZADA\n');		
});