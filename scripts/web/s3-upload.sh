# COMPRIME LOS FILES CON GZIP
echo '>>>>>>>> COMPRIMIENDO FILES CON GZIP';
gzip -9 -r ./dist
node scripts/web/s3-gzip.js

echo '>>>>>>>> DESPLEGANDO EN BUCKET '$BUCKET;

# ELIMINA FILES EN BUCKET S3
cross-env-shell aws s3 rm s3://$BUCKET --recursive

# SUBE FILES CON CACHE
cross-env-shell aws s3 sync ./dist s3://$BUCKET --exclude="index.html" \
                                                --exclude="*/mock/version.json" \
                                                --exclude="*/langs/es.json" \
                                                --exclude="manifest.json" \
                                                --exclude="ngsw.json" \
                                                --exclude="ngsw-worker.js" \
                                                --acl public-read \
                                                --metadata-directive REPLACE \
                                                --cache-control max-age=365000000,immutable,public \
												--content-encoding gzip
# SUBE FILES SIN CACHE											
cross-env-shell aws s3 sync ./dist s3://$BUCKET --exclude="*" \
                                                --include="ngsw-worker.js" \
                                                --acl public-read \
                                                --metadata-directive REPLACE \
                                                --cache-control max-age=0,no-cache,no-store,must-revalidate,public \
												--content-encoding gzip												
# SUBE FILES SIN CACHE - JSON											
cross-env-shell aws s3 sync ./dist s3://$BUCKET --exclude="*" \
                                                --include="*/mock/version.json" \
												--include="*/langs/es.json" \
                                                --include="manifest.json" \
                                                --includ="ngsw.json" \
                                                --acl public-read \
                                                --metadata-directive REPLACE \
                                                --cache-control max-age=0,no-cache,no-store,must-revalidate,public \
												--content-type application/json \
												--content-encoding gzip																						
# SUBE index.html
cross-env-shell aws s3 cp ./dist/index.html s3://$BUCKET \
                                                --acl public-read \
                                                --cache-control public \
												--content-encoding gzip
												
echo '<<<<<<<< FIN DESPLIEGUE';
												
