const fs = require('fs');

const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const editJsonFile = require('edit-json-file');

var indexPath = './dist/index.html';
var versionJsonFile = editJsonFile('./dist/assets/api/mock/version.json');
var valVersion = editJsonFile('./package.json').get('version');

console.log('>>>>>>>> ASIGNANDO LA VERSION DE DESPLIEGUE: '+ valVersion +'\n');

JSDOM.fromFile(indexPath, {}).then(dom => {
	dom.window.document.querySelector('app-root').setAttribute('app-version', valVersion);
	dom.window.document.querySelector('.panel-carga-app .version .value').innerHTML = valVersion;

	fs.writeFile(indexPath, dom.serialize(), null, () => {

		versionJsonFile.set('version', valVersion);
		versionJsonFile.set('compilationTime', new Date().toString());
		versionJsonFile.save();

		console.log('<<<<<<<< ASIGNACION DE VERSION COMPLETADA\n');
	});
});
