var shell 		= require('shelljs');
var devip 		= require('dev-ip');
var readline 	= require('readline');
var getport 	= require('get-port');
var port;

getPort();

function getPort() {	
	getport().then(portValue => {
		port = portValue;
		getIps();
	});
}

function getIps() {
	devip().forEach((ip, index) => {
		shell.echo('IP '+(index+1)+') ' + ip);
	});
	shell.echo('');

	if(devip().length > 1) {
		runReadline();
	} else {
		runServerRed(devip()[0]);
	}
}
 

function runReadline() {
	var rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
		prompt: 'Seleccionar IP nro? > '
	});

	rl.prompt();

	rl.on('line', (line) => {
		var ip = devip()[Number(line.trim()) - 1];		
		if(ip) {
			runServerRed(ip);
			process.exit(0);
		} else {
			rl.prompt();
		}
	});
}

function runServerRed(ip) {
	shell.echo('');
	shell.echo('URL => ' + ip + ':' + port);
	shell.exec('cross-env IP='+ip+' PORT='+port+' npm run server:red');
}