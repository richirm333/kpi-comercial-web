// jQuery
declare var jQuery: any;

// Smartadmin Dependencies
window['jQuery'] = require('jquery');
window['$'] = window['jQuery'];
import 'jquery-ui-npm/jquery-ui.min.js'

require('bootstrap/js/tooltip.js'); // required for X-editable
require('bootstrap/js/popover.js'); // required for X-editable
require('bootstrap/js/dropdown.js'); // required for bootstrap-colorpicker
require('bootstrap/js/tab.js'); //
require('bootstrap/js/modal.js'); //
require('select2/dist/js/select2.min.js');
window['html2canvas'] = require('html2canvas/dist/html2canvas.min.js');

window['moment'] = require('moment');

require('smartadmin-plugins/notification/SmartNotification.min.js');

