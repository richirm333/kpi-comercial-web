import './lib'

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './app/core/constants/app.environment';

if (environment.production) {
  enableProdMode();
}

function bootstrapApp() {
	platformBrowserDynamic().bootstrapModule(AppModule)
	  .catch(err => console.error(err));
}

bootstrapApp()

