import { AppConstants } from '../../app/core/constants/app.constants';
import { environment as env } from '../environment';

export const environment = {
	production: false,
	
	APP_MOCK: 	true,
    DEPLOYMENT:	'dev',
	SERVICE_WORKER: env.SERVICE_WORKER,
};