import { AppConstants } from '../../app/core/constants/app.constants';
import { environment as env } from '../environment';

export const environment = {
	production: true,

    APP_MOCK: 	false,
    DEPLOYMENT:	'dev',
	SERVICE_WORKER: env.SERVICE_WORKER,
};