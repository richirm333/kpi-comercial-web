import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
	
import { IonSliderDirective } from './ion-slider.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [IonSliderDirective],
  exports: [IonSliderDirective],
})
export class IonSliderModule { }
