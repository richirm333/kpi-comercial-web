import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { PanelNavComponent } from './panel-nav.component';

@NgModule({
	imports: [
		RouterModule,
		
		BsDropdownModule,
	],
	declarations: [
		PanelNavComponent,
	],
	exports: [
		PanelNavComponent,
	]
})
export class PanelNavModule {}