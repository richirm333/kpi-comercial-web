import { Component } from '@angular/core';

@Component({
	selector: 'panel-nav',
	templateUrl: './panel-nav.component.html',
	styleUrls: ['./panel-nav.component.css']
})
export class PanelNavComponent {}