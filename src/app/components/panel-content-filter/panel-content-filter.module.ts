import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonSliderModule } from '../ion-slider/ion-slider.module';

import { PanelContentFilterComponent } from './panel-content-filter.component';

@NgModule({
	imports: [
		CommonModule,
		
		IonSliderModule,
	],
	declarations: [
		PanelContentFilterComponent,
	],
	exports: [
		PanelContentFilterComponent,
	]
})
export class PanelContentFilterModule {}