import { Component, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'panel-content-filter',
	templateUrl: './panel-content-filter.component.html',
	styleUrls: ['./panel-content-filter.component.css']
})
export class PanelContentFilterComponent {
	
	@Output()  onSelectFilter: EventEmitter<any> = new EventEmitter();
	
}	