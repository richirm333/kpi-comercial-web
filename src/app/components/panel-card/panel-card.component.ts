import { Component, Input, Output, EventEmitter, TemplateRef } from '@angular/core';

@Component({
	selector: 'panel-card',
	templateUrl: './panel-card.component.html',
	styleUrls: ['./panel-card.component.css']
})
export class PanelCardComponent {
	
	@Input()  title: String;
	@Input()  showFavorite: boolean;
	@Input()  pathShowMore: String;
	@Input()  templateBody: TemplateRef<any>;
	
	favorite: boolean;
	
}