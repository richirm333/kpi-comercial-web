import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PanelCardComponent } from './panel-card.component';

@NgModule({
	imports: [
		RouterModule,
		CommonModule,
	],
	declarations: [
		PanelCardComponent,
	],
	exports: [
		PanelCardComponent,
	]
})
export class PanelCardModule {}
