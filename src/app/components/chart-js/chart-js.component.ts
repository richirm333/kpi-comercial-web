import {Component, AfterContentInit, ElementRef, Input, Output, EventEmitter, OnChanges} from '@angular/core';

import {presets} from './chart-js.presets'

declare var Chart:any;

@Component({
  
  selector: 'chart-js',
  template: `
  <div>
  <canvas></canvas>
  </div>
  `,
  styles: []
})
export class ChartJsComponent implements AfterContentInit, OnChanges {
  
  @Input() public data:any;
  @Input() public type:string;
  @Input() width:string = '100%';
  
  @Input()  options: any = {};
  
  @Input()  render: boolean;
  @Output() renderChange: EventEmitter<any> = new EventEmitter(); 
  
  isLoadJs: boolean;
  
  constructor(private el:ElementRef) {
  }
  
	ngAfterContentInit() {
		Promise.all([
			require('chart.js')
		]).then((chartJs:any)=>{
			this.isLoadJs = true;
			this.renderChart()
		})
	}
  
  ngOnChanges(options) {
	  if(options.render && options.render.currentValue) {
			this.renderChart();
			setTimeout(() => { 
				this.render = false;	
				this.renderChange.emit(false); 
			});
	  }
  }
  
	renderChart(){
		if(this.isLoadJs) {
			let ctx = this.getCtx();
			let data = this.data;

			if(data.datasets && data.datasets.length && presets[this.type] && presets[this.type].dataset){
				data.datasets =  data.datasets.map((it)=>{
					return Object.assign({}, presets[this.type].dataset, it)
				})
			}

			let chart = new Chart(ctx, {
				type: this.type, 
				data: data, 
				options: Object.assign(presets[this.type] ? presets[this.type].options : {}, this.options || {})
			});
			
			chart.update();
		}
	}
  
  private getCtx() {
    return this.el.nativeElement.querySelector('canvas').getContext('2d');
  }
  
  randomScalingFactor() {
    return Math.round(Math.random() * 100);
  };
  randomColorFactor() {
    return Math.round(Math.random() * 255);
  };
  randomColor(opacity) {
    return 'rgba(' + this.randomColorFactor() + ',' + this.randomColorFactor() + ',' + this.randomColorFactor() + ',' + (opacity || '.3') + ')';
  };
  
}
