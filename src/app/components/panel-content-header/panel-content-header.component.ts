import { Component, Input } from '@angular/core';

@Component({
	selector: 'panel-content-header',
	templateUrl: './panel-content-header.component.html',
	styleUrls: ['./panel-content-header.component.css']
})
export class PanelContentHeaderComponent {
	
	@Input()  title: String;
	@Input()  subTitle: String;
	@Input()  category: String;
	
}