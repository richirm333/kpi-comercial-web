import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanelContentHeaderComponent } from './panel-content-header.component';

@NgModule({
	imports: [
		CommonModule,
	],
	declarations: [
		PanelContentHeaderComponent,
	],
	exports: [
		PanelContentHeaderComponent,
	]
})
export class PanelContentHeaderModule {}