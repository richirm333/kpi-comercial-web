import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { environment } from './core/constants/app.environment';

const routes: Routes = [
	{	path: '',
		children: [
			{ 
				path: '', 
				redirectTo: '/auth/login', 
				pathMatch: 'full'
			},
			{ 
				path: 'auth/login', 
				loadChildren: './views/auth/auth.module#AuthModule' 
			},
			{
				path: 'home',
				loadChildren: './views/views.module#ViewsModule',
				// canActivate: [AuthService],
			},
		],
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(
			routes,
			{				
				preloadingStrategy: PreloadAllModules,
				useHash: true
			}
		)
	],
	exports: [RouterModule],
	providers: []
})
export class AppRoutingModule { }
