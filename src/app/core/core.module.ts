import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabsModule, ProgressbarModule, TooltipModule, BsDropdownModule, AlertModule } from 'ngx-bootstrap';

import { throwIfAlreadyLoaded } from './guards/module-import-guard';

@NgModule({
  imports: [
    CommonModule,

    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ProgressbarModule.forRoot(),
    AlertModule.forRoot(),
    TabsModule.forRoot(),
  ],
  providers: [
  ]
})
export class CoreModuleMain {
  constructor( @Optional() @SkipSelf() parentModule: CoreModuleMain) {
    throwIfAlreadyLoaded(parentModule, 'CoreModuleMain');
  }
}
