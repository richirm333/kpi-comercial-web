import { Component, OnInit } from '@angular/core';

import { AppSettings } from './core/constants/app.settings';
import { environment } from './core/constants/app.environment';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
	
	constructor() {}

	ngOnInit() {}
}