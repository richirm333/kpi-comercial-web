import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, FormBuilder } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule } from '@angular/common/http';

import { ProgressHttpModule } from "angular-progress-http";
import { TextMaskModule } from 'angular2-text-mask';

import { BsModalService } from 'ngx-bootstrap/modal';
// import { AuthGuard } from "./core/services/auth-guard.service";

/*
 * Platform and Environment providers/directives/pipes
 */
import { AppRoutingModule } from './app.routing'
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';

// Core providers
import { CoreModuleMain } from "./core/core.module";

// Core providers

import { CommonModule, DatePipe } from "@angular/common";

// Application wide providers
const APP_PROVIDERS = [
	...APP_RESOLVER_PROVIDERS,
	AppState
];

import { environment } from './core/constants/app.environment';
import { AppSettings } from './core/constants/app.settings';

declare var window: any;

type StoreType = {
	state: InternalStateType,
	restoreInputValues: () => void,
	disposeOldHosts: () => void
};

@NgModule({
	bootstrap: [ AppComponent ],
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpClientModule,
		ProgressHttpModule,
		CommonModule,
					
		TextMaskModule,
		CoreModuleMain,		
		AppRoutingModule,
		
		ServiceWorkerModule.register('ngsw-worker.js', { 
			enabled: environment.SERVICE_WORKER,
			registrationStrategy: 'registerWithDelay:5000',
		}),
	],
	exports: [],
	providers: [
		APP_PROVIDERS,

		BsModalService,
		FormBuilder,
	]
})
export class AppModule {
	constructor(
		public appRef: ApplicationRef, 
		public appState: AppState) {
		
	}

}