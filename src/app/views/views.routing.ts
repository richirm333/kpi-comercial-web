import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppConstants } from '../core/constants/app.constants';

import { ViewsComponent } from "./views.component";

export const routes: Routes = [
	{	path: '', 
		component: ViewsComponent,
		children: [
			{ 
				path: '', 
				redirectTo: 'dashboard', 
				pathMatch: 'full' 
			},
			{ 
				path: 'negocio', 
				loadChildren: './negocio/negocio.module#NegocioModule'
			},
			{ 
				path: 'dashboard', 
				loadChildren: './dashboard/dashboard.module#DashboardModule'
			},
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class ViewsRoutingModule { }
