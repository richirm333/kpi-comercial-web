import { Component, OnInit } from '@angular/core';

import { NegocioService } from './negocio.service';
import { NegocioConstants } from './negocio.constants';

declare var moment: any;

@Component({
	selector: 'negocio',
	templateUrl: './negocio.component.html',
	styleUrls: ['./negocio.component.css'],
})
export class NegocioComponent implements OnInit {
	
	totales: any = {};	
	grafico: any = {};
	reload: any = {};
	
	VIEW_CONSTANTS: any = NegocioConstants;
	
	constructor(
		private negocioService: NegocioService) {}
		
	ngOnInit() {
		this.grafico = NegocioConstants.GET_GRAPH_OBJECT();
		
		this.getSales();
	}

	getSales() {
		this.totales.transactions = 0;
		this.totales.transactionsForDay = [];
		
		this.negocioService.getSales().subscribe(
			data => {
				data.forEach(item => {
					this.totales.transactions += item.num_transactions;
					
					this.totales.transactionsForDay[moment(item.date).day()] = 
						(this.totales.transactionsForDay[moment(item.date).day()] || 0) + item.num_transactions;
					
				});
					
				this.calcPercentTransactionForDay();
				// console.log(this.totales);
			}
		);
	}	
	
	calcPercentTransactionForDay() {
		this.totales.percentTransactionForDay = [];
		
		this.totales.transactionsForDay.forEach((value, index) => {
			this.totales.percentTransactionForDay[index] = 
				(100*(this.totales.transactionsForDay[index]/this.totales.transactions)).toFixed(2);
		});
		
		this.totales.percentTransactionForDay.push(0);
		
		this.grafico.datasets[0].data = this.totales.percentTransactionForDay;
		this.reload.grafico = true;
		
	}
	
	onSelectFilter() {
		
	}
}