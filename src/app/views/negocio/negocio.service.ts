import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class NegocioService {
	
	constructor(
		private httpClient: HttpClient) {}
		
	getSales(): Observable<any> {
		return this.httpClient.get('/assets/api/mock/sales.json');
	}
	
}