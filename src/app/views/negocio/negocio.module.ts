import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartJsModule } from '../../components/chart-js/chart-js.module';
import { PanelCardModule } from '../../components/panel-card/panel-card.module';
import { PanelContentHeaderModule } from '../../components/panel-content-header/panel-content-header.module';
import { PanelContentFilterModule } from '../../components/panel-content-filter/panel-content-filter.module';

import { NegocioComponent } from './negocio.component';
import { NegocioService } from './negocio.service';
import { NegocioRoutingModule } from './negocio.routing';

@NgModule({
	imports: [
		CommonModule,
		
		ChartJsModule,
		PanelCardModule,
		PanelContentHeaderModule,
		PanelContentFilterModule,
		
		NegocioRoutingModule,
	],
	declarations: [
		NegocioComponent,
	],
	providers: [
		NegocioService
	]
})
export class NegocioModule {}