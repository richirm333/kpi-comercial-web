/**
  * @dynamic
  */
export class NegocioConstants {
	
	public static GRAPH_OPTIONS: any = {		
		title: {
			display: false, 
			text: 'Transacción promedio'
		},
		legend: {
			display: true
		},
		scales: {
			yAxes: [{
				ticks: {
					callback: function (value) {
						return NegocioConstants.ADD_COMMAS(value)
					}
				},
				scaleLabel: {
					display: true,
					labelString: 'Porcentaje %'
				}
			}]
		},
		tooltips: {
			mode: 'index', 
			intersect: true,
			callbacks: {
				label: function(tooltipItem, chart){
					return ' Mi negocio ' + NegocioConstants.ADD_COMMAS(tooltipItem.yLabel) + '%'
				}
			}
		}		
	}	
		
	public static GET_GRAPH_OBJECT(/*periodos: any = []*/) {
		return {
			labels: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
			datasets: [{
				label: 'Mi negocio',
				backgroundColor: 'rgb(54, 162, 235)',				
				borderColor: 'white',
				borderWidth: 2,
				data: [],
			}]
		}	
	}
	
	public static ADD_COMMAS(nStr: any) {
		nStr += '';
		var x = String(Number(nStr).toFixed(0)).split('.');
		var x1 = x[0];
		var x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}

}