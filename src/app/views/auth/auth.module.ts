import { NgModule } from '@angular/core';

import { AuthComponent } from './auth.component'; 
import { AuthService } from './auth.service'; 
import { AuthRoutingModule } from './auth.routing'; 

@NgModule({
	imports: [
		AuthRoutingModule,
	],
	declarations: [
		AuthComponent,
	],
	providers: [
		AuthService,
	]
})
export class AuthModule {}