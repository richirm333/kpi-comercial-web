export class DashboardConstants {
	
	public static GENDER: any = {
		MALE: 'M',
		FEMALE: 'F'
	};
	
	public static AGE_RANGE: any = [
		[0,9], [10,19], [20,29], [30,39], [40,49], 
		[50,59], [60,69], [70,79], [80, 89], [90, 99]
	];
	
	public static GRAPH_OPTIONS: any = {		
		title: {
			display: false, 
			text: 'Transacción promedio'
		},
		legend: {
			display: true
		},
		scales: {
			yAxes: [{
				ticks: {
					callback: function (value) {
						return DashboardConstants.ADD_COMMAS(value)
					}
				},
				scaleLabel: {
					display: true,
					labelString: 'Valor en S/.'
				}
			}]
		},
		tooltips: {
			mode: 'index', 
			intersect: true,
			callbacks: {
				label: function(tooltipItem, chart){
					return ' Mi negocio S/. ' + DashboardConstants.ADD_COMMAS(tooltipItem.yLabel)
				}
			}
		}		
	}	
		
	public static GET_GRAPH_OBJECT() {
		return {
			labels: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
			datasets: [{
				label: 'Mi negocio S/.',
				backgroundColor: 'rgb(54, 162, 235)',				
				borderColor: 'white',
				borderWidth: 2,
				data: [],
			}]
		}	
	}
	
	public static ADD_COMMAS(nStr: any) {
		nStr += '';
		var x = String(Number(nStr).toFixed(0)).split('.');
		var x1 = x[0];
		var x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}

}