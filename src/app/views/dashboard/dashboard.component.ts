import { Component, OnInit } from '@angular/core';

import { DashboardService } from './dashboard.service';
import { DashboardConstants } from './dashboard.constants';

declare var moment: any;

@Component({
	selector: 'dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
	
	today: String;	
	totales: any = {};	
	grafico: any = {};
	reload: any = {};
	favorites: any = {}
	
	VIEW_CONSTANTS: any = DashboardConstants;
	
	constructor(
		private dashboardService: DashboardService) {}
		
	ngOnInit() {
		this.today = moment().format('DD');		
		
		this.grafico = DashboardConstants.GET_GRAPH_OBJECT();
		
		this.getSales();
		this.getVisitors();
	}

	getSales() {
		this.totales.transactions = 0;
		this.totales.sales = 0;
		
		this.totales.salesForDay = [];
		this.totales.transactionsForDay = [];
		
		this.dashboardService.getSales().subscribe(
			data => {
				data.forEach(item => {
					this.totales.transactions += item.num_transactions;
					this.totales.sales += item.num_sales;
					
					this.totales.salesForDay[moment(item.date).day()] = 
						(this.totales.salesForDay[moment(item.date).day()] || 0) + item.num_sales;
					
					this.totales.transactionsForDay[moment(item.date).day()] = 
						(this.totales.transactionsForDay[moment(item.date).day()] || 0) + item.num_transactions;
					
				});
					
				this.calcSalesTransactionForDay();
				console.log(this.totales);
			}
		);
	}

	getVisitors() {
		this.totales.male = 0;
		this.totales.female = 0;
		
		this.totales.ageRange = {};
				
		this.dashboardService.getVisitors().subscribe(
			data => {
				data.forEach(item => {
					this.totales.male += DashboardConstants.GENDER.MALE == item.gender;
					this.totales.female += DashboardConstants.GENDER.FEMALE == item.gender;
					
					item.age = moment().diff(item.date_of_birth, 'years');
					
					DashboardConstants.AGE_RANGE.forEach(range => {
						if(range[0] <= item.age && item.age <= range[1]) {
							let key = range[0]+' - '+range[1];
							this.totales.ageRange[key] = (this.totales.ageRange[key] || 0) + 1;
						}
					});
				});
				
				this.calcMaxAge();
			}
		);
	}
	
	calcMaxAge() {
		let keyAge;
		let valAge;
		for(let key in this.totales.ageRange) {
			if(!keyAge || this.totales.ageRange[key] > valAge) {
				keyAge = key;
				valAge = this.totales.ageRange[key];
			}
		}
		this.totales.keyMaxAge = keyAge;
		this.totales.valMaxAge = valAge;
	}
	
	calcSalesTransactionForDay() {
		this.totales.salesTransactionForDay = [];
		
		this.totales.salesForDay.forEach((value, index) => {
			this.totales.salesTransactionForDay[index] = 
				this.totales.salesForDay[index].toFixed(2);
		});
		
		this.totales.salesTransactionForDay.push(0);
		
		this.grafico.datasets[0].data = this.totales.salesTransactionForDay;
		this.reload.grafico = true;
		
	}
	
}