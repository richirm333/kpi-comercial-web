import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartJsModule } from '../../components/chart-js/chart-js.module';
import { PanelCardModule } from '../../components/panel-card/panel-card.module';
import { PanelContentHeaderModule } from '../../components/panel-content-header/panel-content-header.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { DashboardRoutingModule } from './dashboard.routing';

@NgModule({
	imports: [		
		CommonModule,
		
		ChartJsModule,
		PanelCardModule,
		PanelContentHeaderModule,
		
		DashboardRoutingModule,
	],
	declarations: [
		DashboardComponent,
	],
	providers: [
		DashboardService,
	]
})
export class DashboardModule {}