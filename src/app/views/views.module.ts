import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanelNavModule } from '../components/panel-nav/panel-nav.module';

import { ViewsRoutingModule } from "./views.routing";
import { ViewsComponent } from './views.component';

@NgModule({
	imports: [
		CommonModule,
		
		PanelNavModule,
		
		ViewsRoutingModule,
	],
	declarations: [
		ViewsComponent,
	],
})
export class ViewsModule { }